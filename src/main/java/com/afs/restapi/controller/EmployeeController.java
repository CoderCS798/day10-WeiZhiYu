package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployees() {
        List<Employee> employees = employeeService.findAll();
        return employees.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {
        Employee targetEmployee = employeeService.findById(id);
        return EmployeeMapper.toResponse(targetEmployee);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public EmployeeResponse updateEmployee(@PathVariable Long id, @RequestBody EmployeeRequest employeeRequest) {
        return EmployeeMapper.toResponse(employeeService.update(id, employeeRequest));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @GetMapping(params = "gender")
    public List<EmployeeResponse> getEmployeesByGender(@RequestParam String gender) {
        List<Employee> targetGenderEmployees = employeeService.findAllByGender(gender);
        return targetGenderEmployees.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee createdEmployee = employeeService.create(employeeRequest);
        return EmployeeMapper.toResponse(createdEmployee);
    }

    @GetMapping(params = {"page", "size"})
    public List<EmployeeResponse> findEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        List<Employee> employeesInPage = employeeService.findByPage(page, size);
        return employeesInPage.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

}
