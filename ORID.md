# O
- Flyway: Flyway is an open source database version management tool. With Flyway you can combine the full power of SQL with solid versioning. This makes setting up and maintaining database schemas a breeze. 
- SpringBoot - Mapper: In the past, the employee class has taken on too many roles, both as an input parameter to the client and as an output to the server. This state makes the program inflexible, and the employee class contains a salary attribute that the server does not want to return to the client, which is a sensitive attribute. The Mapper approach addresses these issues.
- retrospective: In this session, we review recent work and summarize what is well / not well, and put forward some suggestions. Among them, we have summarized several reasons that hinder the team's higher performance: such as not asking for help in time, not thinking about what to do before doing a task.
- presentation: In the afternoon, we also presented concepts such as microservices, containers, and CI CD in groups. Through this presentation, we understood these three technologies and successfully connected them in series to have an overall concept. In addition, TW teacher also briefly introduced Cloud Native, which is closely related to the above three technologies
# R
I am excited.
# I
I learned a lot today, I have a certain understanding of microservices, containers, and CI CD. In addition, a retrospective was made today to summarize the well/not well work done before.
# D
I will continue to do what I did Well and try to improve what I did not well.